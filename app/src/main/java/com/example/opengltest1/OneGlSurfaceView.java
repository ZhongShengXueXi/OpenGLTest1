package com.example.opengltest1;

import android.content.Context;
import android.opengl.GLSurfaceView;

public class OneGlSurfaceView extends GLSurfaceView {
    private final OneGlRenderer mRenderer;
    public OneGlSurfaceView(Context context) {
        super(context);
        // 设置图形库版本，创建了一个OpenGL的渲染线程
        setEGLContextClientVersion(2);
        // 新建一个渲染器
        mRenderer = new OneGlRenderer();
        // 设置一个渲染器
        setRenderer(mRenderer);
//        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
//        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }
}


