【内容】：在Android上，实现一个使用OpenGL显示YUV图像数据的demo

【地址】：https://gitee.com/ZhongShengXueXi/OpenGLTest1.git



【提交】：2019.11.04

【完成】：两部分，1、练习demo，绘制一个三角形；2、显示yuv图像数据。

【待做】：未整合和处理，可读性查，测试的原始代码也都还在，仅供参考。