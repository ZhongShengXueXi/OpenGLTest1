package com.example.opengltest1;

import android.Manifest;
import android.content.pm.PackageManager;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final String tag = "GLES_main";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPermission();

        // 显示YUV数据
        GLSurfaceView glSurfaceView = findViewById(R.id.glSurfaceView);
        glSurfaceView.setEGLContextClientVersion(2);
        GLFrameRenderer glFrameRenderer = new GLFrameRenderer(glSurfaceView);
        glSurfaceView.setRenderer(glFrameRenderer); // 注册渲染器
        glSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        int w = 1920;
        int h = 1080;
        glFrameRenderer.update(w, h);
        byte[] y = new byte[w * h];
//        Arrays.fill(y, (byte)56);   // for test
        byte[] u = new byte[w * h / 4];
        byte[] v = new byte[w * h / 4];

        File file = new File("/sdcard/" + "gf_a_1920x1080.YV12");
        if (! file.exists()) {
            Log.d(tag, "file is not exist");
        } else {
            try {
                FileInputStream in = new FileInputStream(file);
                in.read(y);
                in.read(u);
                in.read(v);
                in.close();
            } catch (FileNotFoundException e) {
                Log.d(tag, "found fail");
                e.printStackTrace();
            } catch (IOException e) {
                Log.d(tag, "read fail");
                e.printStackTrace();
            }
        }

//        file = new File("/sdcard/mono_1920x1080.YV12");
//        try {
//            FileOutputStream out = new FileOutputStream(file);
//            out.write(y);
//            out.write(u);
//            out.write(v);
//            out.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        Log.d(tag, "test");

        glFrameRenderer.update(y, u, v);

        // 新建一个渲染窗口
//        OneGlSurfaceView glSurfaceView = new OneGlSurfaceView(this);
        // 为活动设置一个渲染窗口
//        setContentView(glSurfaceView);

    }

    private void checkPermission() {
        String[] permissions = new String[]{
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        };
        List<String> mPermissionList = new ArrayList<>();

        mPermissionList.clear();
        for (String permission1 : permissions) {
            if (ContextCompat.checkSelfPermission(MainActivity.this, permission1) != PackageManager.PERMISSION_GRANTED) {
                mPermissionList.add(permission1);
            }
        }
        if (mPermissionList.isEmpty()) {//未授予的权限为空，表示都授予了
            Toast.makeText(MainActivity.this, "已经授权", Toast.LENGTH_LONG).show();
        } else {//请求权限方法
            String[] permission = mPermissionList.toArray(new String[0]);//将List转为数组
            ActivityCompat.requestPermissions(MainActivity.this, permission, 2);
        }
    }

    // 数据格式转换
    private IntBuffer intBufferUtil(int[] arr)
    {
        IntBuffer mBuffer;
        // 初始化ByteBuffer，长度为arr数组的长度*4，因为一个int占4个字节
        ByteBuffer qbb = ByteBuffer.allocateDirect(arr.length * 4);
        // 数组排列用nativeOrder
        qbb.order(ByteOrder.nativeOrder());
        mBuffer = qbb.asIntBuffer();
        mBuffer.put(arr);
        mBuffer.position(0);
        return mBuffer;
    }
    private FloatBuffer floatBufferUtil(float[] arr)
    {
        FloatBuffer mBuffer;
        // 初始化ByteBuffer，长度为arr数组的长度*4，因为一个int占4个字节
        ByteBuffer qbb = ByteBuffer.allocateDirect(arr.length * 4);
        // 数组排列用nativeOrder
        qbb.order(ByteOrder.nativeOrder());
        mBuffer = qbb.asFloatBuffer();
        mBuffer.put(arr);
        mBuffer.position(0);
        return mBuffer;
    }
    private ShortBuffer shortBufferUtil(short[] arr)
    {
        ShortBuffer mBuffer;
        // 初始化ByteBuffer，长度为arr数组的长度*2，因为一个short占2个字节
        ByteBuffer dlb = ByteBuffer.allocateDirect(
                // (# of coordinate values * 2 bytes per short)
                arr.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        mBuffer = dlb.asShortBuffer();
        mBuffer.put(arr);
        mBuffer.position(0);
        return mBuffer;
    }

}
